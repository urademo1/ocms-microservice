FROM node:16-alpine
WORKDIR /usr/src/app
COPY package*.json ./

RUN yarn install
COPY . .
EXPOSE 8080

CMD [ "node", "src/index.js" ]