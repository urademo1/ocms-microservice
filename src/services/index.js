const users = require('./users/users.service.js');
const notices = require('./notices/notices.service.js');
const assignments = require('./assignments/assignments.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(notices);
  app.configure(assignments);
}
