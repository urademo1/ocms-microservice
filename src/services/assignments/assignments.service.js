// Initializes the `assignments` service on path `/assignments`
const { Assignments } = require('./assignments.class');
const createModel = require('../../models/assignments.model');
const hooks = require('./assignments.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/assignments', new Assignments(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('assignments');

  service.hooks(hooks);
};
