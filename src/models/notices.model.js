// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get("sequelizeClient");
  const notices = sequelizeClient.define(
    "notices",
    {
      offenseDate: { type: DataTypes.DATE },
      noticeNo: { type: DataTypes.STRING },
      offense: { type: DataTypes.STRING },
      carpark: { type: DataTypes.STRING },
      location: { type: DataTypes.STRING },
      vehicleNo: { type: DataTypes.STRING },
      PersonFurnished: { type: DataTypes.STRING },
      dateFurnished: { type: DataTypes.STRING },
      amount: { type: DataTypes.DOUBLE },
      dueDate: { type: DataTypes.DATE },
      status: { type: DataTypes.STRING },
      userID: { type: DataTypes.STRING },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line no-unused-vars
  notices.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return notices;
};
