// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get("sequelizeClient");
  const assignments = sequelizeClient.define(
    "assignments",
    {
      identificationType: { type: DataTypes.STRING },
      identification: { type: DataTypes.STRING },
      name: { type: DataTypes.STRING },
      contact: { type: DataTypes.STRING },
      email: { type: DataTypes.STRING },
      postal: { type: DataTypes.STRING },
      address: { type: DataTypes.STRING },
      relationship: { type: DataTypes.STRING },
      othersReason: { type: DataTypes.STRING },
      leasedStartDate: { type: DataTypes.DATE },
      leasedEndDate: { type: DataTypes.DATE },
      files: { type: DataTypes.STRING },
      noticeID: { type: DataTypes.STRING },
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line no-unused-vars
  assignments.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return assignments;
};
